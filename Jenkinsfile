node {
    // config 
    def commit_id
    def to = emailextrecipients([
            [$class: 'CulpritsRecipientProvider'],
            [$class: 'DevelopersRecipientProvider'],
            [$class: 'RequesterRecipientProvider']
    ])
    def server = Artifactory.server 'artifactory'
    def mavenContainer = docker.image('maven:3.6.1-jdk-8-alpine')
    mavenContainer.pull()   

    // job
    try {
        // cloning git repo
        stage('prep') {
            git url: 'https://gitlab.com/luiyivp/Ear-Test.git'
            sh "git rev-parse --short HEAD > .git/commit-id"                        
            commit_id = readFile('.git/commit-id').trim()
        }

        // scanning code with sonarqube
        stage('sonar-scanner') {
            def sonarqubeScannerHome = tool name: 'sonar', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
            withCredentials([string(credentialsId: 'sonar', variable: 'sonarLogin')]) {
                sh "${sonarqubeScannerHome}/bin/sonar-scanner -e -Dsonar.host.url=http://sonarqube:9000 -Dsonar.login=${sonarLogin} -Dsonar.projectName=Ear-Test -Dsonar.projectVersion=${env.BUILD_NUMBER} -Dsonar.projectKey=ET -Dsonar.sources=testWAR/src/ -Dsonar.language=java -Dsonar.java.binaries=."
            }
        }

        // building project with maven
        stage('build') {
            mavenContainer.inside("-v /root/.m2:/root/.m2") {
                sh 'mvn -B -DskipTests clean package'
            }
        }

        // pushing to jfrog artifactory
        stage('publish') {
            def uploadSpec = """{
            "files": [
                {
                "pattern": "testEAR/target/test.ear",
                "target": "libs-release-local/Ear-Test.ear"
                }
             ]
            }"""
            server.upload(uploadSpec)
        }

        // build docker image based on wildfly and pushing to local docker registry
        stage('docker build/push') {
            docker.withRegistry('http://registry:5000') {
            def app = docker.build("luiyi/test-ear:${commit_id}", '.').push()
            }
        }

    } catch(e) {
        // mark build as failed
        currentBuild.result = "FAILURE";
        // set variables
        def subject = "${env.JOB_NAME} - Build #${env.BUILD_NUMBER} ${currentBuild.result}"
        def content = '${JELLY_SCRIPT,template="html"}'

        // send email
        if(to != null && !to.isEmpty()) {
        emailext(body: content, mimeType: 'text/html',
            replyTo: '$DEFAULT_REPLYTO', subject: subject,
            to: to, attachLog: true )
        }

        // mark current build as a failure and throw the error
        throw e;
    }
}